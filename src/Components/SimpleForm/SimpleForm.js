import React, { useState } from "react"
import { Grid, TextField, FormControlLabel, FormControl, FormLabel, Select, Button, MenuItem, RadioGroup, Radio, Text, Typography } from "@material-ui/core"
import { render } from "@testing-library/react"

const SimpleForm = () => {

  const defaultFormValues = {
    firstName: "",
    lastName: "",
    gender: "",
    age: 0
  }

  const [formValues, setFormValues] = useState(defaultFormValues)
  const [isSubmitted, setIsSubmitted] = useState(false)

  const handleInputChange = (e) => {
    const { name, value } = e.target
    setFormValues({
      ...formValues,
      [name]: value
    })
  }

  const handleSubmit = (e) => {
    e.preventDefault()
    console.log(formValues);
    setIsSubmitted(!isSubmitted)
  }

  return (
    <form onSubmit={handleSubmit}>
      <Grid container alignItems="center" justify="center" direction="column">
        <Grid item>
          <TextField id="firstName" name="firstName" label="firstName" type="text" value={formValues.firstName} onChange={handleInputChange}>First Name</TextField>
        </Grid>
        <Grid item>
          <TextField id="lastName" name="lastName" label="lastName" type="text" value={formValues.lastName} onChange={handleInputChange}>Last Name</TextField>
        </Grid>
        <Grid item>
          <TextField id="age" name="age" label="age" type="number" value={formValues.age} onChange={handleInputChange}>Age</TextField>
        </Grid>
        <Grid item>
          <Typography>Are you a...</Typography>
          <RadioGroup name="gender" value={formValues.gender} onChange={handleInputChange} row>
            <FormControlLabel key="boy" value="boy" control={<Radio size="small" />} label="Boy" />
            <FormControlLabel key="girl" value="girl" control={<Radio size="small" />} label="Girl" />
            <FormControlLabel key="neither" value="neither" control={<Radio size="small" />} label="Neither!" />
          </RadioGroup>
        </Grid>
        <Button variant="contained" color="primary" type="submit">
          {isSubmitted ? `Hide Results` : `Submit`}
        </Button>
      </Grid>
      <Typography id="results" hidden={!isSubmitted}>{`The values gathered are: ${JSON.stringify(formValues, null, 2)}`}</Typography>
    </form>
  )

}

export default SimpleForm